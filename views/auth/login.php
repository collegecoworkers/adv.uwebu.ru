<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->params['breadcrumbs'][] = $this->title;
?>
<br>
<br>
<br>
<br>
<br>
<br>

<!-- banner -->
<div class="inside-banner">
  <div class="container text-center"> 
    <h2><?= $this->title ?></h2>
  </div>
</div>
<!-- banner -->

<div class="container">
    <div class="row  justify-content-md-center">
      <div class="col-md-6 col-md-auto">
				<?php 
					$form = ActiveForm::begin([
						'id' => 'login-form',
					]);
				?>

				<?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

				<?= $form->field($model, 'password')->passwordInput() ?>

				<?= $form->field($model, 'rememberMe')->checkbox([
					'template' => "<div>{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
					'style' => 'height: 13px;',
				])
				?>

				<?= Html::submitButton('Отправить', ['class' => 'btn btn-success' ]) ?>

				<?php ActiveForm::end(); ?>
      </div>
    </div>
</div>

<br>
<br>
<br>
<br>
<br>
