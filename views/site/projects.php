<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<!-- Page Content -->
<div class="container">

	<!-- Page Heading/Breadcrumbs -->
	<h1 class="mt-4 mb-3">Все обьявления
	</h1>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/">Главная</a>
		</li>
		<li class="breadcrumb-item active">Все обьявления</li>
	</ol>

	<?php foreach($projects as $item):?>
		<!-- Blog Post -->
		<div class="card mb-4">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-6">
						<a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>">
							<img class="img-fluid rounded" src="<?= $item->getImage() ?>" alt="">
						</a>
					</div>
					<div class="col-lg-6">
						<h2 class="card-title"><?= $item->title ?></h2>
						<p class="card-text"><?= $item->description ?></p>
						<a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>" class="btn btn-primary">Подробнее &rarr;</a>
					</div>
				</div>
			</div>
			<div class="card-footer text-muted">
				Цена: 
				<?= $item->price ?>
			</div>
		</div>
	<?php endforeach ?>

	<ul class="pagination justify-content-center">
		<?php
		echo LinkPager::widget([
			'pagination' => $pagination,
		]);
		?>
	</ul>

</div>